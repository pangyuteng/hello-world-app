#!/bin/bash

DOCKER_BUILDKIT=1 docker build -t hello-world-app:latest .
docker tag hello-world-app:latest pangyuteng/hello-world-app:latest
echo ${DOCKERHUB_ACCESS_TOKEN} | docker login -u pangyuteng --password-stdin
docker push pangyuteng/hello-world-app:latest